package HomeWork;
public class HomeWork51 {
    public HomeWork51() {
    }

    public static void main(String[] args) {
        int[] array = new int[]{345, 298, 456, -1};
        int minDigit = 10;

        for(int i = 0; i < array.length - 1; ++i) {
            for(int a = array[i]; a > 0; a /= 10) {
                int curDigit = a % 10;
                if (curDigit < minDigit) {
                    minDigit = curDigit;
                }
            }
        }

        System.out.println(minDigit);
    }
}